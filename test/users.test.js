const supertest = require('supertest');
const app = require('../app');

var key = "";
//Sentencia
describe('Probar todo el sistema de autenticación', () =>{
  //Casos de prueba => 50% (1 prueba del caso de prueba que acierte y otra que falle)
  it('Deberia de obtener un login con usuario y contraseña correctos.', (done)=>{
    supertest(app).post('/login')
    .send({'email': 'dprez@uach.mx', 'password':'abcd1234'})
    .expect(200)
    .end(function(err, res){
      key = res.body.objs;
      done();
    });
  });
});


describe('Probar las rutas de los usuarios', ()=>{
  it('Deberia de obtener la lista de usuarios', (done) => {
    supertest(app).get('/users/')
    .set('Authorization', `Bearer ${key}`)
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        //expect(res.body.objs.limit).toEqual(5);
        expect(res.statusCode).toEqual(200)
        //expect(res.body.message).equal("Usuarios del sistema")
        done();
      }
    });
  })
})
